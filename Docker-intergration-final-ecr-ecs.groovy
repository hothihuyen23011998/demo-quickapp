pipeline {
  agent { node {label 'master'}}
  parameters {
    choice(
      name: 'envs2',
      choices: ['Production','Staging'],
      description: 'choices Production or Staging'
    )
    string(name: 'REGION', defaultValue: 'us-east-2', description: '')
    // choice(
    //   name: 'Method',
    //   choices: ['Update-image','Create new service'],
    //   description: 'choices Update image OR Create new service'
    // )
  }
  environment {
    IMAGE_FE         = "301733143684.dkr.ecr.us-east-2.amazonaws.com/my-quickapp-fe"
    IMAGE_BE         = "registry.gitlab.com/hothihuyen23011998/docker-registry/quickapp-be"
    IMAGE_BE_TAG     = "${IMAGE_BE}:${BUILD_NUMBER}"
    IMAGE_FE_TAG     = "${IMAGE_FE}:${BUILD_NUMBER}"
    DOCKERFILE_BE    = "Dockerfile"
    DOCKERFILE_FE    = "./QuickApp/ClientApp/Dockerfile"
    CONTEXT_BE       = "."
    CONTEXT_FE       = "./QuickApp/ClientApp/"
    IMAGE_DB         = "mcr.microsoft.com/mssql/server:2019-latest"
    GIT_URL          = "https://gitlab.com/hothihuyen23011998/demo-quickapp.git"
    GIT_CREDENTIAL   = "huyen-gitlab"
    WORK_DIR         = "/media/huyen/Elements/jenkins"
  }
  stages {
      
            stage('checkout') {
              steps {
                checkout(
                  [$class: 'GitSCM', branches: [[name: "${envs2}" ]], doGenerateSubmoduleConfigurations: false,
                  gitTool: 'jgitapache', submoduleCfg: [],
                  userRemoteConfigs: [[credentialsId: "${GIT_CREDENTIAL}",url:"${GIT_URL}"]]]
                    )
                   }
              }


        //       stage ('Report To Sonarqube') {
        //        steps {
        //        sh '''
        //            cd QuickApp
        //            #dotnet tool install --global dotnet-sonarscanner
        //            dotnet test
        //            dotnet sonarscanner begin /k:"quickapp" /d:sonar.host.url="http://23.96.27.77:9000"  /d:sonar.login="7320ddc74fde3be3d7132a81d462e6e6d09a47ad"
        //            dotnet build
        //            dotnet sonarscanner end /d:sonar.login="7320ddc74fde3be3d7132a81d462e6e6d09a47ad"
        //        '''
        //    }
        // }

              stage('Test') {
                  steps {
                        sh 'pwd'
                        sh '''
                        cd $WORK_DIR/workspace/Quickapp-ECS
                        rm -r nUnit.Tests
                        dotnet new nunit -o nUnit.Tests
                        dotnet sln add ./nUnit.Tests/nUnit.Tests.csproj
                        dotnet test ./nUnit.Tests/nUnit.Tests.csproj --logger:trx -r nUnit.Tests/allure-results
                        #allure generate nUnit.Tests/TestResults -o $WORK_DIR/allure-report --clean
                        #allure serve nUnit.Tests/TestResults
                '''
                      }
                  }
              stage('Reports to allure report') {
                  steps {
                      sh 'export PATH="$PATH:/allure/bin"'
                      script {
                          allure([
                            includeProperties: false,
                            jdk: '',
                            properties: [],
                            reportBuildPolicy: 'ALWAYS',
                            results: [[path: 'nUnit.Tests/allure-results']]
                        ])
                      }
                  }
              }

      
      stage('Build Webapi') {
          steps {
            sh '''
            DOCKER_BUILDKIT=1 docker build --no-cache --tag ${IMAGE_BE}:${BUILD_NUMBER} --file ${DOCKERFILE_BE} --network host ${CONTEXT_BE}
            '''
          }
      }
      stage('Build Frontend') {
          environment {
                        TARGET_ENVIRONMENT = params.envs2.toLowerCase()
                        }
          steps {
            sh '''
            export DOCKER_BUILDKIT=0
            export COMPOSE_DOCKER_CLI_BUILD=0
            docker build --build-arg ENVIROMENT=${TARGET_ENVIRONMENT} --tag ${IMAGE_FE}:${BUILD_NUMBER} --file ${DOCKERFILE_FE} --network host ${CONTEXT_FE}
            '''
          }
      }
      // stage('Push image Webapi to ECR') {
      //     steps {
      //       withAWS(credentials: 'aws-hohuyen-key', region: "${params.REGION}") {
      //         sh "eval $(aws ecr get-login --region us-east-2 --no-include-email)"
      //         sh  "docker push ${IMAGE_BE}:${BUILD_NUMBER}"
      //       }
      //     }
      // }
      stage('Push image Frontend to ECR') {
          steps {
            withAWS(credentials: 'aws-hohuyen-key', region: "${params.REGION}") {
              sh '''
                eval $(aws ecr get-login --region us-east-2 --no-include-email)
              '''
              sh "docker push ${IMAGE_FE}:${BUILD_NUMBER}"
              
            }
          }
      }
      
      // stage ("Deploy to ECS") {
      //   steps {
      //     withAWS(credentials: 'aws-hohuyen-key', region: "${params.REGION}") {
      //         sh '''
      //           pwd && ls -la
      //           cd docker-ecr-ecs && ls -la
      //           terraform init && terraform validate
      //           #terraform apply --target aws_ecs_service.myapp-service --var MYAPP_SERVICE_ENABLE="1" --var MYAPP_VERSION=${BUILD_NUMBER} --auto-approve
      //           terraform destroy --target aws_ecs_service.webapp-myapp-service --var MYAPP_SERVICE_ENABLE="1" --var MYAPP_VERSION=${BUILD_NUMBER} --auto-approve
      //           terraform apply --target aws_ecs_service.webapp-myapp-service --var MYAPP_SERVICE_ENABLE="1" --var MYAPP_VERSION=${BUILD_NUMBER} --auto-approve
      //         '''
      //       }
      //   }


      // }

    }

  }

