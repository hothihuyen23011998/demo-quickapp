[
  {
    "essential": true,
    "memory": 256,
    "name": "quickapp-webapp",
    "cpu": 256,
    "image": "${REPOSITORY_URL}:${APP_VERSION}",
    "entryPoint": [
            "sh",
            "-c"
         ],
    "workingDirectory": "/",
    "command": ["/docker-entrypoint.sh"],
    "portMappings": [
        {
            "containerPort": 80,
            "hostPort": 80
        }
    ],
    "environment": [
          {
              "name": "ASPNETCORE_URLS",
              "value": "http://*:80"
          }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": { 
          "awslogs-group" : "/ecs/frontend-container",
          "awslogs-region": "us-east-2"
        }
      }
  }
]

