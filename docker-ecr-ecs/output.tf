# output "backend-elb" {
#   value = aws_elb.myapp-be-elb.dns_name
# }
output "frontend-elb" {
  value = aws_elb.myapp-fe-elb.dns_name
}

output "quickapp-frontend-repositor-URL" {
  value = "${aws_ecr_repository.fe.repository_url}"
}

# output "quickapp-backend-repositor-URL" {
#   value = "${aws_ecr_repository.be.repository_url}"
# }
