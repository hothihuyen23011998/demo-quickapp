variable "MYAPP_SERVICE_ENABLE" {
  default = "0"
}

variable "MYAPP_VERSION" {
  default = "latest"
}

variable "AWS_REGION" {
  default = "us-east-2"
}

variable "ECS_INSTANCE_TYPE" {
  default = "t2.micro"
}

variable "ECS_AMIS" {
  type = map(string)
  default = {
    us-east-2 =  "ami-062be0c2f0e7fb6d2"
    ap-northeast-2 = "ami-0a09f75d88cac17d3"
    ap-southeast-1 = "ami-0c5ab0d956378c44d"
  }
}
# Full List: http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html

variable "AMIS" {
  type = map(string)
  default = {
    us-east-2 =  "ami-062be0c2f0e7fb6d2"
    ap-northeast-2 = "ami-0a09f75d88cac17d3"
    ap-southeast-1 = "ami-0c5ab0d956378c44d"
  }
}
