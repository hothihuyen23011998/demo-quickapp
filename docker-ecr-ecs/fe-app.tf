# app

data "template_file" "myapp-task-definition-template" {
  template = file("templates/fe-app.json.tpl")
  vars = {
    REPOSITORY_URL = replace(aws_ecr_repository.fe.repository_url, "https://", "")
    APP_VERSION    = var.MYAPP_VERSION
  }
}

resource "aws_ecs_task_definition" "myapp-task-definition" {
  family                = "quickapp-webapp"
  container_definitions = data.template_file.myapp-task-definition-template.rendered
}

resource "aws_ecs_service" "webapp-myapp-service" {
  count           = var.MYAPP_SERVICE_ENABLE
  name            = "quickapp-webapp"
  cluster         = aws_ecs_cluster.example-cluster.id
  task_definition = aws_ecs_task_definition.myapp-task-definition.arn
  desired_count   = 1
  iam_role        = aws_iam_role.ecs-service-role.arn
  depends_on      = [aws_iam_policy_attachment.ecs-service-attach1]
  launch_type = "EC2"
  load_balancer {
    elb_name       = aws_elb.myapp-fe-elb.name
    container_name = "quickapp-webapp"
    container_port = 80
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}

# load balancer
resource "aws_elb" "myapp-fe-elb" {
  name = "quickapp-angular-webapp-elb"

  # access_logs {
  #   bucket        = "tfstate-huyenht-docker-ecr-ecs-123123123"
  #   bucket_prefix = "quickapp-fe"
  #   interval      = 60
  # }

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }


  listener {
    instance_port      = 80
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "arn:aws:acm:us-east-2:301733143684:certificate/62191876-db78-4622-8784-31108c645417"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 20
    target              = "HTTPS:443/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  subnets         = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id]
  security_groups = [aws_security_group.myapp-elb-securitygroup.id]

  tags = {
    Name = "quickapp-webapp-elb"
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "/ecs/frontend-container"
}
