export const environment = {
    production: true,
    baseUrl: 'https://localhost:8002', // Change this to the address of your backend API if different from frontend address
    tokenUrl: 'https://localhost:8002/connect/token', // For IdentityServer/Authorization Server API. You can set to null if same as baseUrl
    loginUrl: '/login'
};
