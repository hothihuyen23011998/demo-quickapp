export const environment = {
    production: true,
    baseUrl: 'https://huyen-prod-be.byhanh.com', // Change this to the address of your backend API if different from frontend address
    tokenUrl: 'https://huyen-prod-be.byhanh.com/connect/token', // For IdentityServer/Authorization Server API. You can set to null if same as baseUrl
    loginUrl: '/login'
};
