[
  {
    "essential": true,
    "memory": 256,
    "name": "myapp",
    "cpu": 256,
    "image": "${REPOSITORY_URL}:${APP_VERSION}",
    "workingDirectory": "/app",
    "portMappings": [
        {
            "containerPort": 6002,
            "hostPort": 6002
        }
    ]
  }
]

