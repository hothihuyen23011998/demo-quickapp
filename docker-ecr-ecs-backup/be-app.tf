# app

data "template_file" "myapp-task-definition-template" {
  template = file("templates/be-app.json.tpl")
  vars = {
    REPOSITORY_URL = replace(aws_ecr_repository.be.repository_url, "https://", "")
    APP_VERSION    = var.MYAPP_VERSION
  }
}

resource "aws_ecs_task_definition" "myapp-task-definition" {
  family                = "myapp-backend"
  container_definitions = data.template_file.myapp-task-definition-template.rendered
}

resource "aws_ecs_service" "myapp-service" {
  count           = var.MYAPP_SERVICE_ENABLE
  name            = "myapp-backend"
  cluster         = aws_ecs_cluster.example-cluster.id
  task_definition = aws_ecs_task_definition.myapp-task-definition.arn
  desired_count   = 1
  iam_role        = aws_iam_role.ecs-service-role.arn
  depends_on      = [aws_iam_policy_attachment.ecs-service-attach1]

  load_balancer {
    elb_name       = aws_elb.myapp-be-elb.name
    container_name = "quickapp-webapi"
    container_port = 6002
  }
  lifecycle {
    ignore_changes = [task_definition]
  }
}

# load balancer
resource "aws_elb" "myapp-be-elb" {
  name = "quickapp-webapi-elb"

  listener {
    instance_port     = 6002
    instance_protocol = "https"
    lb_port           = 443
    lb_protocol       = "https"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 30
    target              = "HTTPS:6002/"
    interval            = 60
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  subnets         = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id]
  security_groups = [aws_security_group.myapp-elb-securitygroup.id]

  tags = {
    Name = "quickapp-backend-elb"
  }
}

