pipeline {
  agent { node {label 'master'}}
  parameters {
    choice(
      name: 'envs',
      choices: ['Production','Staging'],
      description: 'choices Production or Staging'
    )
    choice(
      name: 'envs2',
      choices: ['Production','Staging'],
      description: 'choices Production or Staging'
    )
  }
  environment {
    IMAGE_FE         = "registry.gitlab.com/hothihuyen23011998/docker-registry/angular-nginx"
    IMAGE_BE         = "registry.gitlab.com/hothihuyen23011998/docker-registry/quickapp"
    IMAGE_BE_TAG     = "${IMAGE_BE}:${BUILD_NUMBER}"
    IMAGE_FE_TAG     = "${IMAGE_FE}:${BUILD_NUMBER}"
    DOCKERFILE_BE    = "Dockerfile"
    DOCKERFILE_FE    = "./QuickApp/ClientApp/Dockerfile"
    CONTEXT_BE       = "."
    CONTEXT_FE       = "./QuickApp/ClientApp/"
    IMAGE_DB         = "mcr.microsoft.com/mssql/server:2019-latest"
    GIT_URL          = "https://gitlab.com/hothihuyen23011998/demo-quickapp.git"
    GIT_CREDENTIAL   = "huyen-gitlab"
  }
  stages {
        stage("Checkout") {
                  steps {
                    cleanWs()
                    checkout(
                      [$class: 'GitSCM', branches: [[name: "master" ]], doGenerateSubmoduleConfigurations: false,
                      gitTool: 'jgitapache', submoduleCfg: [],
                      userRemoteConfigs: [[credentialsId: "${GIT_CREDENTIAL}",url:"${GIT_URL}"]]]
                    )
                  }
              }
        //stage ('Report To Sonarqube') {
        //        steps {
        //        sh '''
        //            cd QuickApp
        //            #dotnet tool install --global dotnet-sonarscanner
        //            dotnet test
        //            dotnet sonarscanner begin /k:"quickapp" /d:sonar.host.url="http://23.96.27.77:9000"  /d:sonar.login="7320ddc74fde3be3d7132a81d462e6e6d09a47ad"
        //            dotnet build
        //            dotnet sonarscanner end /d:sonar.login="7320ddc74fde3be3d7132a81d462e6e6d09a47ad"
        //        '''
        //    }
        //}
        stage('Test') {
                  steps {
                        sh 'pwd'
                        sh '''
                        cd /var/lib/jenkins/workspace/QuickApp-Dockerize
                        #rm -r nUnit.Tests
                        #dotnet new nunit -o nUnit.Tests
                        #dotnet sln add ./nUnit.Tests/nUnit.Tests.csproj
                        dotnet test ./nUnit.Tests/nUnit.Tests.csproj --logger:trx -r nUnit.Tests/allure-results
                        #allure generate nUnit.Tests/TestResults -o $WORK_DIR/allure-report --clean
                        #allure serve nUnit.Tests/TestResults
                '''
                      }
                  }
        stage('Reports to allure report') {
                  steps {
                      sh 'export PATH="$PATH:/allure/bin"'
                      script {
                          allure([
                            includeProperties: false,
                            jdk: '',
                            properties: [],
                            reportBuildPolicy: 'ALWAYS',
                            results: [[path: 'nUnit.Tests/allure-results']]
                        ])
                      }
                  }
              }

      stage('Build Webapi') {
          steps {
            sh '''
            # docker prune system -a 
            DOCKER_BUILDKIT=1 docker build --tag ${IMAGE_BE}:${BUILD_NUMBER} --file ${DOCKERFILE_BE} --network host ${CONTEXT_BE}
            '''
          }
      }
      stage('Build Frontend') {
          environment {
                        TARGET_ENVIRONMENT = params.envs2.toLowerCase()
                        }
          steps {
            sh '''
            export DOCKER_BUILDKIT=0
            export COMPOSE_DOCKER_CLI_BUILD=0
            docker build --build-arg ENVIROMENT=${TARGET_ENVIRONMENT} --tag ${IMAGE_FE}:${BUILD_NUMBER} --file ${DOCKERFILE_FE} --network host ${CONTEXT_FE}
            '''
          }
      }
      stage('Push image Webapi to registry') {
          steps {
            sh '''
            docker push ${IMAGE_BE}:${BUILD_NUMBER}
            '''
          }
      }
      stage('Push image Frontend to registry') {
          steps {
            sh '''
            docker push ${IMAGE_FE}:${BUILD_NUMBER}
            '''
          }
      }
                stage('Service up') {
           steps {
             sh '''
             set -e; IMAGE_BE_TAG=${IMAGE_BE_TAG}
             set -e; IMAGE_FE_TAG=${IMAGE_FE_TAG}
             '''
             sh "docker-compose -f docker-compose.yml -f docker-compose-'${params.envs}'.yml up -d --build"
           }
       }


      
        stage ("Deploy to other Agent") {
          agent { node {label "$envs2" } }
          stages {
              stage("Checkout_repo") {
                  steps {
                    checkout(
                      [$class: 'GitSCM', branches: [[name: "${envs2}" ]], doGenerateSubmoduleConfigurations: false,
                      gitTool: 'jgitapache', submoduleCfg: [],
                      userRemoteConfigs: [[credentialsId: "${GIT_CREDENTIAL}",url:"${GIT_URL}"]]]
                    )
                  }
              }
              stage("Pull Images") {
                  steps {
                    sh '''
                    docker login registry.gitlab.com
                    docker pull ${IMAGE_BE_TAG}
                    docker pull ${IMAGE_FE_TAG}
                    '''
                  }
              }
              stage('Deploy') {
                  steps {
                    sh '''
                    set -a; IMAGE_BE_TAG=${IMAGE_BE_TAG}
                    set -a; IMAGE_FE_TAG=${IMAGE_FE_TAG}
                    '''
                    sh "docker-compose -f docker-compose.yml -f docker-compose-'${params.envs2}'.yml up -d --build"
                    sh "docker-compose ps -a"
                  }
              }
          }
      }
      


    }
}

