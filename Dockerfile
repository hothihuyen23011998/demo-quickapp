FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
ENV ASPNETCORE_Kestrel__Certificates__Default__Path=/app/QuickApp.pfx 
ENV ASPNETCORE_HTTPS_PORT=6002
ENV ASPNETCORE_Kestrel__Certificates__Default__Password=huyen123
ENV ASPNETCORE_URLS=http://*:6001;https://*:6002
RUN apt update && apt install nano net-tools iputils-ping telnet -y
EXPOSE 6001 6002

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
RUN apt update && apt install nano net-tools iputils-ping telnet -y
COPY ["QuickApp/QuickApp.csproj", "QuickApp/"]
COPY ["DAL/DAL.csproj", "DAL/"]
RUN dotnet restore "QuickApp/QuickApp.csproj"
COPY . .
WORKDIR "/src/QuickApp"
RUN dotnet build  "QuickApp.csproj" -c Release -o /app/build
RUN dotnet dev-certs https --clean
RUN dotnet dev-certs https -ep ./QuickApp.pfx -p huyen123
COPY ./QuickApp .

#test ./QuickApp/QuickApp.pfx

FROM build AS publish
RUN dotnet publish "QuickApp.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /src/QuickApp/QuickApp.pfx ./QuickApp.pfx
ENTRYPOINT ["dotnet", "QuickApp.dll", "watch", "run", "--no-restore", "--urls", "http://0.0.0.0:6001"]


#FROM nginx:1.21.0
#RUN apt update && apt install nano certbot python3-certbot-nginx net-tools iputils-ping telnet -y
#COPY quickappbe /etc/nginx/sites-enabled
#RUN certbot run -n --nginx --agree-tos -d huyen-stag.byhanh.com -m hothihuyen23011998@gmail.com --redirect
#RUN nginx -s reload
